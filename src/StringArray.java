import java.util.Scanner;

public class StringArray {
    String array[]=new String[7];
    public void initializeArray(){
        System.out.println("Enter names:");
        Scanner scanner=new Scanner(System.in);
        for (int iterate = 0; iterate < array.length; iterate++) {
            array[iterate]=scanner.next();
        }
    }
    public void displayStringArray(){
        for (int iterate = 0; iterate < array.length; iterate++) {
            System.out.println(array[iterate]);
        }
    }

}
